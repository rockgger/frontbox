import './Styles.scss';
import Header from './components/Header'
import Carousel from './components/Carousel'
import 'bootstrap/dist/css/bootstrap.min.css'
import VideoPlayer from './components/VideoPlayer';
import Carousel2 from './components/Carousel2'
import News from './components/News'
import SubscriptionBar from './components/SubscriptionBar'
import SpecialContent from './components/SpecialContent'
import WeatherBar from './components/WeatherBar'
import ProceduresNServices from './components/ProceduresNServices'
import Calendar from './components/Calendar'
import Footer from './components/Footer'

import './slick/slick.min.css'
import './slick/slick-theme.min.css'

export default function App() {


  return (
    <div className="App">

      <Header />

      <Carousel />

      <VideoPlayer />

      <Carousel2 />

      <News />

      <SubscriptionBar />

      <SpecialContent />

      <WeatherBar />

      <ProceduresNServices />

      <Calendar />

      <Footer />

    </div>
  );
}