import { Fragment } from 'react'

export default function VideoPlayer (props) {
  return (
    <Fragment>
      <div className='multimedia-div'>
        <h2>Módulo para multimedia y Estadístico</h2>
        <video className='mt-4' controls>
          <source src="mov_bbb.mp4" type="video/mp4"/>
          <source src="mov_bbb.ogg" type="video/ogg"/>
          Your browser does not support HTML video.
        </video>
      </div>
    </Fragment>
  )
}