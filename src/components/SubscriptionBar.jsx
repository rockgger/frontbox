import digitalMarketing from '../img/06-digital-marketing.svg'

export default function SubscriptionBar(){
  return(
    <div className='subscription-bar-div'>
      <div className='group-1 mb-3'>
        <div>
          <img src={digitalMarketing} alt='' className='subscr-icon' />
        </div>
        <div>
          <div className='subscr-txt-1'>
            <span>Suscríbete </span>
            a nuestros boletines
          </div>
          <div className='subscr-txt-2'>
            <input type="checkbox" id="term-cond" name="term-cond" value="accept"/>
            <label htmlFor="term-cond">Al dar click en SUSCRIBIRME AHORA acepta los 
            <span className='text-tc'> términos y condiciones</span></label>
          </div>
        </div>
      </div>
      
      <div className='mb-3'>
        <input type='email' className='txt-email' placeholder='Ingresa tu correo electrónico'/>
      </div>
      <div className='mb-3'>
        <button>CALL TO ACTION</button>
      </div>
    </div>
  )
}