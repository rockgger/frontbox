import { useEffect, useState } from 'react'
import thermometer from '../img/08-thermometer.svg'
import { Col, Dropdown, DropdownButton, Row } from 'react-bootstrap'

export default function WeatherBar() {
  const [degrees, setDegrees] = useState(0)
  const [zone, setZone] = useState({})
  const [dropdownOptions, setDropdownOptions] = useState(null)
  const status = {
    moderate: <span><span className='status-moderate ms-2'></span> Moderada</span>,
    critical: <span><span className='status-critical ms-2'></span> Crítica</span>,
  }

  useEffect(() => {
    setDegrees(28)
    setZone({
      neighborhood: 'San Javier',
      no2: 1,
      pm10: 1,
    })
    setDropdownOptions(
      <>
        <Dropdown.Item href="#/action-1">Action 1</Dropdown.Item>
        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
      </>
    )
  }, [])

  return (
    <div className='weather-bar-div'>

      <Row>
        <Col lg={5}>
          <div className='weath-group-1'>
    
            <div className='ml-4'>
              <img src={thermometer} alt='' />
            </div>
    
            <div>
              <legend className='weather-txt-1'>
                <strong>Estado del tiempo: </strong> 
                  <span className='d-inline-flex align-items-baseline'>
                    Clima Actual:
                    <span className='degrees'>&nbsp;{degrees} º C</span>
                  </span>
              </legend>
            </div>
    
          </div>
        </Col>
        <Col lg={7}>
          <div className='weath-group-2'>

            <div className='weather-txt-2'>
              <span>
                Calidad del&nbsp;<strong>Aire:&nbsp;&nbsp;</strong>
              </span>
              <span>
                  <span className='d-inline-flex'>
                    {zone.neighborhood} |&nbsp;
                  </span>
                  <span 
                    className='d-inline-flex' >
                    <strong>NO<sub>2</sub> </strong>
                    {zone.no2 === 1 ? status.moderate : status.critical}&nbsp;&nbsp;
                  </span>
                <span className='d-inline-flex'>
                  <strong>PM<sub>10</sub> </strong>
                  {zone.pm10 === 1 ? status.moderate : status.critical}
                  <DropdownButton className='ms-3' variant="default" title="">
                    {dropdownOptions}
                  </DropdownButton>
                </span>
              </span>
            </div>

          </div>
        </Col>
      </Row>

    </div>
  )
}