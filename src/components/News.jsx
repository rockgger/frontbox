import { useEffect, useState } from 'react'
import {
  Nav,
  Navbar,
} from 'react-bootstrap'
import image from '../img/image.png'
import audio from '../img/05-audio.svg'
import gallery from '../img/05-gallery.svg'
import play from '../img/05-play.svg'


export default function News(){
  const [news, setNews] = useState([])

  useEffect(() => {
    let news = []
    for (let i = 1; i <= 4; i++) {
      news.push(
        {
          title: `Título Principal de la Noticia  ${i}`,
          author: 'Nombre del autor o secretaría',
          date: 'Fecha de publicación',
          img: image,
          altImg: '',
          text: `Texto descriptivo o introducción a contenido para informar, 
            promover o posicionar contenidos, servicios o beneficios. Debe 
            contener un Call to Action que direccione a contenido de consumo 
            para el usuario final.`
        }
      )
    }
    setNews(news)
  }, []) 

  return(
    <div className='news-div'>
      <Navbar 
        id='nav-3'
        bg="light" 
        variant="light"
        expand="lg"
      >
        <Nav.Link href="#" id='news-title' style={{paddingLeft: '0'}}>
          <span>Noticias</span> contenido relevante
        </Nav.Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
        <Nav className="mr-auto">
          <Nav.Link href="#">Últimas Publicaciones</Nav.Link>
          <Nav.Link href="#">Noticias más leídas</Nav.Link>
          <Nav.Link href="#" id="all-news-link">VER TODAS LAS NOTICIAS</Nav.Link>
        </Nav>
        </Navbar.Collapse>
      </Navbar>

      <hr />

      <div className='news-body'>
        <div className='left-news-div'>
          <h4>
            {
              news && news[0] && news[0].title ?
              news[0].title
              : ''
            }
          </h4>
          <span style={{fontWeight: 'bold'}}>Por</span> {news && news[0] && news[0].author ? news[0].author : ''} | {news && news[0] && news[0].date ? news[0].date : ''}
          <legend className='display-block categ-txt-1 mt-3'>Categorías de contenido</legend>
          <legend className='display-block categ-txt-1'>
            <span class="categ-parent">
              <span className='categ-1'></span> Categoría 1
            </span>
            <span class="categ-parent">
              <span className='categ-2'></span> Categoría 2
            </span>
            <span class="categ-parent">
              <span className='categ-3'></span> Categoría 3
            </span>
          </legend>
          <img 
            src={
              news && news[0] && news[0].img ? 
              news[0].img : ''} 
            alt={news && news[0] && news[0].altImg ? news[0].altImg : ''} 
            className='news-img mt-3'/>
          <p className='mt-3'>{news && news[0] && news[0].text ? news[0].text : ''}</p>
          <span className='close-span'>CERRAR <span>&times;</span></span>
          <button>CALL TO ACTION</button>
          <div className='right-news-bottom display-flex mt-2'>
            <div className='news-txt-3'>
              Tipo de lectura Aprox:
            </div>
            <div className='news-functions'>
              <div>
                <img src={audio} alt='' />
                <legend>Escuchar audio</legend>
              </div>
              <div>
                <img src={gallery} alt='' />
                <legend>Galería</legend>
              </div>
              <div>
                <img src={play} alt='' />
                <legend>Ver la noticia</legend>
              </div>
            </div>
          </div>
        </div>
        
        <div className='right-news-div'>
        {news.map((n, index) => 
          <div key={index} className='right-news'>
            <div>
              <img src={n.img || ''} alt={n.altImg || ''}/>
            </div>
            <div>
              <h4>{n.title || ''}</h4>
              <span style={{fontWeight: 'bold'}}>Por</span> {n.author || ''} | {n.date || ''}
              <legend className='display-block categ-txt-2 mt-3'>Categorías de contenido</legend>
              <legend className='display-block categ-txt-2'>
                <span class="categ-parent">
                  <span className='categ-1'></span> Categoría 1
                </span>
                <span class="categ-parent">
                  <span className='categ-2'></span> Categoría 2
                </span>
                <span class="categ-parent">
                  <span className='categ-3'></span> Categoría 3
                </span>
              </legend>
              <p className='mt-3'>{n.text || ''}</p>
              <div className='display-flex space-between'>
                <div>  <button>CALL TO ACTION</button>  </div>
                <div>  <div class="plus-circle">+</div>  </div>
              </div>
            </div>
          </div>
          ).slice(1,3)}
        </div>
      </div>

    </div>
  )
}