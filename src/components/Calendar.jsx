import { useEffect, useState } from 'react'
import {
  Nav,
  Navbar,
} from 'react-bootstrap'
import image from '../img/image.png'

function getDate(date) {
  const monthNames = [
    'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
  ]
  const dateArr = {
    // day: (new Date(date || '').getDate()<10?'0':'') + new Date(date || '').getDate(),
    day: ((new Date(date || '').getDate() + 1) < 10 ? '0' : '') + (new Date(date || '').getDate() + 1),
    month: (monthNames[new Date(date || '').getMonth()] || ''),
    year: new Date(date || '').getFullYear()
  }
  return dateArr
};

export default function Calendar() {
  const [events, setEvents] = useState([])

  useEffect(() => {
    let events = []
    for (let i = 1; i <= 7; i++) {
      events.push(
        {
          title: 'Título del contenido',
          category: 'Categoría o agrupador',
          date: '2021-09-20',
          img: image,
          altImg: '',
          text: `Descripción o introducción al contenido o detalle del 
            voluntariado. El comportamiento de éste módulo se basa en 
            categorizar el contenido.`
        }
      )
    }
    setEvents(events)
  }, [])

  return (
    <div className='calendar-div'>
      <Navbar
        id='nav-calen'
        bg="light"
        variant="light"
        expand="md"
      >
        <Nav.Link href="#" id='calendar-title'>
          <strong>Calendario de Eventos y Convocatorias</strong> Alcaldía de Medellín
        </Nav.Link>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Nav className="mr-auto">
            <Nav.Link href="#" id="whole-calendar-link">VER TODO EL CALENDARIO</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <hr />


      <Navbar
        id='nav-months'
        bg="light"
        variant="light"
        expand="xl"
      >
        <Navbar.Brand href="#">


          <Nav.Link href="#" id='calendar-year'><strong>2021</strong></Nav.Link>


        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav className="mr-auto">
            <Nav.Link href="#">Enero</Nav.Link>
            <Nav.Link href="#">Febrero</Nav.Link>
            <Nav.Link href="#">Marzo</Nav.Link>
            <Nav.Link href="#">Abril</Nav.Link>
            <Nav.Link href="#">Mayo</Nav.Link>
            <Nav.Link href="#">Junio</Nav.Link>
            <Nav.Link href="#">Julio</Nav.Link>
            <Nav.Link href="#">Agosto</Nav.Link>
            <Nav.Link href="#">Septiembre</Nav.Link>
            <Nav.Link href="#">Octubre</Nav.Link>
            <Nav.Link href="#">Noviembre</Nav.Link>
            <Nav.Link href="#">Diciembre</Nav.Link>
          </Nav>


          <Nav.Link href="#" id="calendar-search">BUSCAR</Nav.Link>


        </Navbar.Collapse>
      </Navbar>



      <div>
        <div className='event-calendar-div'>
          <div className='calen-img'>
            <div className='event-date'>
              {
                events && events[0] && events[0].date ?
                <>
                  <span className='date-month'>{getDate(events[0].date).month}</span>
                  <span className='date-day'>{getDate(events[0].date).day}</span>
                  <span className='date-year'>{getDate(events[0].date).year}</span>
                </>
                : ''
              }
            </div>
            <div>
              <img 
                src={events && events[0] && events[0].img ? events[0].img : ''} 
                alt={events && events[0] && events[0].altImg ? events[0].altImg : ''} 
                className='mt-3' 
              />
            </div>
          </div>

          <div className='event-body'>
            <span className='event-category'>
              {
                events && events[0] && events[0].category ?
                  events[0].category
                  : ''
              }
            </span>
            <h4>
              {/* {
                events && events[0] && events[0].title ?
                events[0].title
                : ''
              } */}
              Título del evento
            </h4>
            {/* <p className='mt-3'>{events && events[0] && events[0].text ? events[0].text : ''}</p> */}
            <p className='mt-3'>
              Descripción o introducción al contenido o detalle del
              voluntariado. El comportamiento de éste módulo se basa
              en categorizar el contenido, el usuario podrá dar clic
              en la categoría: Capacitaciones, voluntariados o charlas,
              y el sistema mostrará el contenido por categoría.
            </p>
            <button>CALL TO ACTION</button>
          </div>
        </div>



        <div className='calend-div-bottom'>
          {events.map((n, index) =>
            <div key={index} className='event-card'>
              <div className='calen-img'>
                <div className='event-date'>
                  {
                    events && events[0] && events[0].date ?
                    <>
                      <span className='date-month'>{getDate(events[0].date).month}</span>
                      <span className='date-day'>{getDate(events[0].date).day}</span>
                      <span className='date-year'>{getDate(events[0].date).year}</span>
                    </>
                    : ''
                  }
                </div>
                <div>
                  <img 
                    src={events && events[0] && events[0].img ? events[0].img : ''} 
                    alt={events && events[0] && events[0].altImg ? events[0].altImg : ''} 
                    className='mt-3' 
                  />
                </div>
              </div>

              <span className='event-category'>
                {
                  events && events[0] && events[0].category ?
                    events[0].category
                    : ''
                }
              </span>
              <h4>
                {
                  events && events[0] && events[0].title ?
                    events[0].title
                    : ''
                }
              </h4>


              <p className='mt-3'>{events && events[0] && events[0].text ? events[0].text : ''}</p>

              <button className='mt-3'>CALL TO ACTION</button>

            </div>
          ).slice(1, 4)}
        </div>


      </div>

    </div>
  )
}