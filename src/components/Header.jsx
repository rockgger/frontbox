import { useState } from 'react'
import govLogo from '../img/gov-logo.svg'
import alcaldiaLogo from '../img/alc_-_portal-de-la-ciudad.png'
import escudoCirc from '../img/escudo-circ-2x.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faMale, 
  faChevronDown, 
  faGlobeAmericas,
  faSearch,
} from '@fortawesome/free-solid-svg-icons'
import { Navbar,Nav,NavDropdown,Form,FormControl,Button } from 'react-bootstrap'


export default function Header() {
  const [menuOptions, setMenuOptions] = useState(
    [
      'Opción Menú 1',
      'Opción Menú 2',
      'Opción Menú 3',
      'Opción Menú 4',
      'Opción Menú 5',
    ]
  )


  return (
    <header className="app-header">

          <Navbar
            id='nav-1'
            bg="primary" 
            variant="dark"
            expand="lg"
            sticky="top">
            <Navbar.Brand href="https://www.gov.co/">
              <img src={govLogo} alt='Gobierno Colombia Logo' width='100px'/>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
              <Nav className="mr-auto">
                <NavDropdown 
                  title={
                    <>
                      <FontAwesomeIcon icon={faMale} />
                      &nbsp;&nbsp;&nbsp;Opciones de Accesibilidad&nbsp;&nbsp;
                    </>
                  } 
                  id="basic-nav-dropdown1"
                >
                  <NavDropdown.Item href="#">Opción 1</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 2</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 3</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown 
                  title={
                    <>
                      <FontAwesomeIcon icon={faGlobeAmericas} />
                      &nbsp;&nbsp;&nbsp;Español&nbsp;&nbsp;
                    </>
                  } 
                  id="basic-nav-dropdown2"
                >
                  <NavDropdown.Item href="#">Opción 1</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 2</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 3</NavDropdown.Item>
                </NavDropdown>
                <NavDropdown 
                  title={
                    <>
                      <img src={escudoCirc} alt='Escudo Alcaldía' width='35px'/>
                      <span>&nbsp;&nbsp;&nbsp;Alcaldía de Medellín |</span>
                      &nbsp;Secretarías y Dependencias&nbsp;&nbsp;
                    </>
                  } 
                  id="basic-nav-dropdown3">
                  <NavDropdown.Item href="#">Opción 1</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 2</NavDropdown.Item>
                  <NavDropdown.Item href="#">Opción 3</NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Navbar>


          
          <Navbar 
            id='nav-2'
            bg="light" 
            variant="light"
            expand="lg"
          >
            <Navbar.Brand href="#home">
              <div>
                <img src={alcaldiaLogo} alt="Logo alcaldía"/>
              </div>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
            <Nav className="mr-auto">
              <Nav.Link href="#">Opción Menú 1</Nav.Link>
              <Nav.Link href="#">Opción Menú 2</Nav.Link>
              <Nav.Link href="#">Opción Menú 3</Nav.Link>
              <Nav.Link href="#">Opción Menú 4</Nav.Link>
              <Nav.Link href="#">Opción Menú 5</Nav.Link>
            </Nav>
            <div className="search-div">
              <span className="userbox-search"><FontAwesomeIcon icon={faSearch} /></span>
            </div>
            </Navbar.Collapse>
          </Navbar>


    </header>
  );
}