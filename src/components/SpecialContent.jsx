import React, { Component } from 'react'
import Slider from 'react-slick'
import image from '../img/image.png'

export default class SpecialContent extends Component {
  constructor(){
    super()
    this.state = {
      cards: [],
      activeSlide: 0,
      itemsPagination: null,
      slidesToShow: 3,
    }
    this.next = this.next.bind(this)
    this.previous = this.previous.bind(this)
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  componentDidMount(){
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
    let cards = []
    let itemsPagination = []
    for (let i = 1; i <= 7; i++) {
      cards.push(
      {
        title: `Título ${i}`,
        img: image,
        altImg: '',
        text: `Texto descriptivo o introducción a contenido para informar, 
          promover o posicionar contenidos, servicios o beneficios`
      })
      itemsPagination.push(i)
    }
    this.setState({cards, itemsPagination})
  }

  resize() {
    let width450 = (window.innerWidth <= 450);
    if(width450){
      this.setState({slidesToShow: 1})
    } else {
      this.setState({slidesToShow: 3})
    }
  }

  componentWillUnmount() {
      window.removeEventListener('resize', this.resize.bind(this));
  }

  render(){
    const settings = {
      arrows: false,
      className: 'center spec-content-card',
      centerMode: true,
      infinite: true,
      // centerPadding: "60px",
      slidesToShow: this.state.slidesToShow,
      speed: 500,
      
      beforeChange: (current, next) => this.setState({ activeSlide: next }),
    }

    return(
      <div className='special-content-div'>
        <h2 className='special-content-title mb-6'><span>Módulo</span> Contenido especial</h2>

        <div className='special-content-container display-flex space-between' >

        <div  className='special-content-left'>
          <Slider ref={c => (this.slider = c)} {...settings}>
            {this.state.cards.map((card, index) => 
              <div key={index}>
                <div>
                  <img src={card.img || ''} alt={card.altImg || ''}/>
                </div>
                <div>
                  <h4>{card.title || ''}</h4>
                  <p className='mt-3'>{card.text || ''}</p>
                    <div>  <button>CALL TO ACTION</button>  </div>
                </div>
              </div>
              )}
          </Slider>
        </div>

        <div  className='special-content-right'>
          <h2 className='special-content-title'><span>Título o nombre</span></h2>
          <p>
            Texto descriptivo o introducción a contenido para informar, 
            promover o posicionar contenidos, servicios o beneficios. 
            <br /><br />
            Debe contener un Call to Action que direccione a contenido de 
            consumo para el usuario final.
          </p>
          <div className='nav-pagin display-flex space-between'>
            <span className='spec-content-dots-prev' onClick={this.previous}>INICIO</span>
            <span className='spec-content-dots-numbers'>
              {
                this.state.itemsPagination && this.state.itemsPagination.length ?
                this.state.itemsPagination.map((item, i) => <span key={i} id={`num-${i}`}>{item}</span>)
                : null
              }
            </span>
            <span className='spec-content-dots-next' onClick={this.next}>SIGUIENTE</span>
          </div>
        </div>

        </div>

        <style>{`
          #num-${this.state.activeSlide} {
            color: orange;
            font-size: 25px;
            font-weight: bold;
          }
        `}</style>
      </div>
    )
  }
}