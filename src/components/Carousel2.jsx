import React, { Component, Fragment } from 'react';
import Slider from 'react-slick'
import carouselImg1 from '../img/04-bus.svg'
import carouselImg2 from '../img/04-building.svg'
import carouselImg3 from '../img/04-study.svg'
import carouselImg4 from '../img/04-friends.svg'
import leftArrow from '../img/arrow-left.svg'
import rightArrow from '../img/arrow-right.svg'


export default class Carousel2 extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [
        {
          src: carouselImg1,
          alt: 'Slide 1',
          number: '90%',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
        {
          src: carouselImg2,
          alt: 'Slide 2',
          number: '750',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
        {
          src: carouselImg3,
          alt: 'Slide 3',
          number: '1.500',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
        {
          src: carouselImg4,
          alt: 'Slide 4',
          number: '12.000',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
        {
          src: carouselImg2,
          alt: 'Slide 5',
          number: '750',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
        {
          src: carouselImg3,
          alt: 'Slide 6',
          number: '100',
          caption: 'Cifra o dato estadístico relevante de impacto a la ciudad en esfuerzo del Conglomerado'
        },
      ],
      slidesToShow: 5,
    }
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }
  
  componentDidMount() {
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
  }

  resize() {
    let width300 = (window.innerWidth <= 300);
    let width450 = (window.innerWidth <= 450);
    let width600 = (window.innerWidth <= 600);
    let width768 = (window.innerWidth <= 768);
    if(width300){
      this.setState({slidesToShow: 1})
    } else if(width450){
      this.setState({slidesToShow: 2})
    } else if(width600){
      this.setState({slidesToShow: 3})
    } else if(width768){
      this.setState({slidesToShow: 4})
    } else {
      this.setState({slidesToShow: 5})
    }
  }

  componentWillUnmount() {
      window.removeEventListener('resize', this.resize.bind(this));
  }

  render() {
    const settings = {
      arrows: false,
      infinite: true,
      slidesToShow: this.state.slidesToShow,
    }
    return (
      <Fragment>
        <div className='carous-2-div'>
          
          <div className='carous-2-arrow' onClick={this.previous}><img src={leftArrow} /></div>

          <div className='carous-2-center'>
            <div className='carous-2'>
              <Slider ref={c => (this.slider = c)} {...settings}>
                {this.state.items.map((item, index) =>
                  <div key={index} className='carous-2-card'>
                    <div>
                      <img src={item.src || ''} alt={item.alt || ''} className='carous-2-img' />
                    </div>
                    <div>
                      <h4 className='carous-2-number'>{item.number || ''}</h4>
                      <p className='mt-3'>{item.caption || ''}</p>
                    </div>
                  </div>
                )}
              </Slider>
            </div>
          </div>

          <div className='carous-2-arrow' onClick={this.next}><img src={rightArrow} /></div>

        </div>
      </Fragment>
    )
  }
}