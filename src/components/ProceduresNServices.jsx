import React, { Component } from 'react'
import {
  Nav,
  Navbar,
} from 'react-bootstrap'
import Slider from 'react-slick'
import invoice from '../img/09-invoice.svg'
import bill from '../img/09-bill.svg'
import license from '../img/09-license.svg'


export default class ProceduresNServices extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      slidesToShow: 4,
    }
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  componentDidMount() {
    window.addEventListener('resize', this.resize.bind(this));
    this.resize();
    this.setState({
      items: [
        {
          img: license,
          altImg: '',
          title: 'Estado de cuenta',
          text: `Texto explicativo o descriptivo del tipo de contenido o 
          acciones que se enontrará.`,
        },
        {
          img: bill,
          altImg: '',
          title: 'Estado de cuenta',
          text: `Texto explicativo o descriptivo del tipo de contenido o 
          acciones que se enontrará.`,
        },
        {
          img: invoice,
          altImg: '',
          title: 'Estado de cuenta',
          text: `Texto explicativo o descriptivo del tipo de contenido o 
          acciones que se enontrará.`,
        },
        {
          img: bill,
          altImg: '',
          title: 'Estado de cuenta',
          text: `Texto explicativo o descriptivo del tipo de contenido o 
          acciones que se enontrará.`,
        },
        {
          img: license,
          altImg: '',
          title: 'Estado de cuenta',
          text: `Texto explicativo o descriptivo del tipo de contenido o 
          acciones que se enontrará.`,
        },]
    })
  }

  resize() {
    let width450 = (window.innerWidth <= 450);
    if(width450){
      this.setState({slidesToShow: 2})
    } else {
      this.setState({slidesToShow: 4})
    }
  }

  componentWillUnmount() {
      window.removeEventListener('resize', this.resize.bind(this));
  }

  render() {
    const settings = {
      dots: true,
      arrows: false,
      infinite: true,
      slidesToShow: this.state.slidesToShow,
    }

    return (
      <div className='proced-serv-div'>
        <Navbar
          id='nav-p-n-s'
          // bg="dark"
          variant="dark"
          expand="lg"
        >
          <Navbar.Brand href="#" id='proced-serv-title'>
            Trámites y Servicios para la ciudadanía
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
            <Nav className="mr-auto">
              <Nav.Link href="#" id='white-link-1'>Trámites</Nav.Link>
              <Nav.Link href="#" id='white-link-2'>Servicios</Nav.Link>
              <Nav.Link href="#" id="all-proced-serv-link">
                TODOS LOS TRÁMITES Y SERVICIOS
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>


        <hr />

        <legend>
          Texto descriptivo o explicativo de la sección que visualiza el
          visitante al portal. Breve, claro y con cercanía a nivel de la
          introducción.
        </legend>

        <div className='proced-serv-body'>
            {
              <div className='ps-carous-div'>
                <div className='ps-carous--center'>
                  <div className='ps-carous'>
                    <Slider ref={c => (this.slider = c)} {...settings}>
                      {this.state.items.map((item, index) =>
                        <div key={index} className='ps-carous-card'>
                          <div>
                            <img src={item.img || ''} alt={item.altImg || ''} className='ps-carous-img' />
                          </div>
                          <div>
                            <h4 className='ps-carous-title'>{item.title || ''}</h4>
                            <p className='mt-3'>{item.text || ''}</p>
                            <div>  <button>REALIZAR TRÁMITE</button>  </div>
                            <a href='#'>Conoce más...</a>
                          </div>
                        </div>
                      )}
                    </Slider>
                  </div>
                </div>
              </div>
            }
        </div>

      </div>
    )
  }
}