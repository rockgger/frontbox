import React, { Component, Fragment } from 'react';
import image from '../img/image.png'

import Slider from 'react-slick'
import leftArrow from '../img/arrow-left.svg'
import rightArrow from '../img/arrow-right.svg'

export default class CarouselHome extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
    }
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
  }

  next() {
    this.slider.slickNext();
  }
  previous() {
    this.slider.slickPrev();
  }

  componentDidMount(){
    let items = []
    for (let i = 1; i <= 4; i++) {
      items.push(
        {
          title: `${process.env.REACT_APP_TITLE} ${i}`,
          subtitle: `Subtítulo de contenido ${i}`,
          img: image,
          altImg: '',
          text: `Texto descriptivo o introducción a contenido para informar, 
            promover o posicionar contenidos, servicios o beneficios. Debe 
            contener un Call to Action que direccione a contenido de consumo 
            para el usuario final.`
        }
      )
    }
    this.setState({ items })
  }

  render() {
    const settings = {
      dots: true,
      arrows: false,
      infinite: true,
    }
    return (
      <Fragment>

        <div className='carous-1-div'>

          <div className='carous-1-left-arrow' onClick={this.previous}>
            <img src={leftArrow} alt=''/>
          </div>

          <div className='carous-1-center'>
            <div className='carous-1'>
              <Slider ref={c => (this.slider = c)} {...settings}>
                {
                  this.state.items.map((item, index) => 
                    <div key={index}>
                      <div className='carous-container'>
                        <div className='left-div'>
                          <div>
                            <legend className='carous-title'>
                              <strong>{item.title || ''}</strong>
                            </legend>
                            <legend className='carous-subtitle'>{item.subtitle || ''}</legend>
                            <legend className='display-block categ-txt-1 mt-3'>Categorías de contenido</legend>
                            <legend className='display-block categ-txt-1'>
                              <span className='categ-parent'>
                                <span className='categ-1'></span>&nbsp; Categoría 1
                              </span>
                              <span className='categ-parent'>
                                <span className='categ-2'></span>&nbsp; Categoría 2
                              </span>
                              <span className='categ-parent'>
                                <span className='categ-3'></span>&nbsp; Categoría 3
                              </span>
                            </legend>
                            <br />
                            <p className='mt-4'>{item.text || ''}</p>
                            <div className='display-flex space-between'>
                              <div>  <button>CALL TO ACTION</button>  </div>
                            </div>
                          </div>
                        </div>
                        <div className='right-div'>
                          <img 
                            src={item.img || ''} 
                            alt={item.altImg || ''}
                          />
                        </div>
                      </div>
                    </div>
                  )
                }
              </Slider>
            </div>
          </div>

          <div className='carous-1-right-arrow' onClick={this.next}><img src={rightArrow} /></div>

        </div>

      </Fragment>
    );
  }
}