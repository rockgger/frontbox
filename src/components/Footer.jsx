import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn,
  faYoutube,
} from '@fortawesome/free-brands-svg-icons'
import { faHeadset } from '@fortawesome/free-solid-svg-icons'
import logoAlc from '../img/alc-logo-black-text.svg'
import chatImg from '../img/11a-chat.png'
import handsImg from '../img/11a-hands.png'
import intranetImg from '../img/11a-intranet.png'
import phonoImg from '../img/11a-phono.png'
import asocapitalesImg from '../img/11b-asocapitales.png'
import col_timeImg from '../img/11b-col_time.png'
import compra_eficienteImg from '../img/11b-compra_eficiente.png'
import correoremotoImg from '../img/11b-correoremoto.png'
import gob_lineaImg from '../img/11b-gob_linea.png'
import radioImg from '../img/11b-radio.png'
import logoCol from '../img/logo_colombia.png'
import logoGov from '../img/gov-logo.svg'

export default function Footer() {
  return (
    <footer>
      <div className='bg-grey'></div>

      <div className='footer-div'>



        <section className='footer-section-1'>

          <div className='section-1-1'>

            <div>
              <h4>Horarios de atención</h4>
              <div class="informacionFoot">
                <p>
                  <strong>Horario de atención: </strong>
                  <br />
                  <strong>Centro Administrativo Municipal: </strong>
                  <br />
                  Lunes a jueves (ingreso) 7:30am a 12:00m y 1:30pm a 5:00pm.
                  <br />
                  Viernes (ingreso): 7:30am a 12:00m y 1:30pm a 4:00pm.
                  <br />
                  <strong>
                    Oficinas de Servicios Tributarios La Alpujarra: </strong><br />
                    Lunes a jueves 7:30am a 5:00pm. <br />
                    Viernes: 7:30am a 4:00pm.<br />
                  <strong>
                    Centro de Servicios a la Ciudadanía La Alpujarra - Sótano:
                      <br />
                  </strong>
                    Lunes a Jueves de 7:30 am a 5:30 pm y viernes 7:30am a 4:30pm (jornada continua).
                    <br /> <br />
                  <strong>Dirección: </strong><br />
                    Calle 44 N 52 – 165 Centro Administrativo La Alpujarra – Medellín, Colombia.
                    <br /> <br /> <strong>Sedes externas: </strong><br />
                  <a
                    href="/irj/portal/medellin?NavigationTarget=inicio/AtencionCiudadana"
                    title="Sedes"
                  >Sedes</a>
                  <br />
                </p>
                <p>
                  <strong>Correo portal:</strong> <br />
                  <a href="https://mercurio.medellin.gov.co/mercurio/inicialPqr.jsp" target="_blank">Contáctenos</a>
                </p>
                <p>
                  <strong>Correo notificaciones judiciales: </strong><br />
                  <a href="mailto:notimedellin.oralidad@medellin.gov.co">notimedellin.oralidad@medellin.gov.co</a>
                  <br /> <br />
                </p>
              </div>
            </div>

            <div className='section-1-1-1'>
              <img src={logoAlc} alt='Logo Alcaldía de Medellín' />
              <div className='networks'>
                <legend>Síguenos en las redes</legend>
                <FontAwesomeIcon icon={faFacebookF} />
                <FontAwesomeIcon icon={faInstagram} />
                <FontAwesomeIcon icon={faTwitter} />
                <FontAwesomeIcon icon={faLinkedinIn} />
                <FontAwesomeIcon icon={faYoutube} />
              </div>
            </div>

          </div>

          <div className='section-1-2'>

            <p>
              <a
                href="#"
                target="_blank"
              >
                VER MÁS INFORMACIÓN
                </a>
            </p>


            <p className='links'>
              <a
                href="/irj/go/km/docs/documents/ServiciosLinea/PoliticasDeUsoPortal.pdf"
                target="_blank"
              >
                Política de privacidad y condiciones de uso
                </a>
                &nbsp;&nbsp;&nbsp;
                <a
                href="/irj/go/km/docs/wpccontent/Sites/Subportal del Ciudadano/Atención Ciudadana/Secciones/Publicaciones/Documentos/2012/Políticas actualización web.pdf"
                target="_blank">Política editorial y de actualización
                </a>
              <br />
              <a
                href="/irj/portal/medellin?NavigationTarget=navurl://2ad28d7ff8a64a54ce04056638630291"
                title="Carta de trato digno a la ciudadanía"
                target="_blank"
              >
                Carta de trato digno a la ciudadanía
                </a>
                &nbsp;&nbsp;&nbsp;
                <a
                href="/irj/portal/medellin?NavigationTarget=navurl://0c1fac2d10e8310090ff005de7c0c77b"
                title="Protección de datos personales en el Municipio de Medellín"
              >
                Protección de datos personales en el Municipio de Medellín
                </a>
            </p>

          </div>

        </section>



        <section className='footer-section-2'>

          <div className='section-2-1'>

            <div>
              <p>
                <strong>Notificaciones:</strong>
                <br />
                <a href="/irj/portal/medellin?NavigationTarget=navurl://767504fca8916d6150ad76cb19658430">Publicaciones, citaciones, comunicaciones y notificaciones</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin/consulnoti">Notificaciones actos administrativos</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=contenido/394-Proyectos-normativos">Agenda regulatoria</a>
              </p>
              <p>
                <strong>Ayudas:</strong>
                <br />
                <a href="https://correo.medellin.gov.co/owa" target="_blank">Correo remoto</a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://outlook.office.com/mail/inbox" title="Correo 365">Correo 365</a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://csc.medellin.gov.co/USDKV8" title="CSC - Centro de Servicios Compartidos" target="_blank">CSC - Centro de Servicios Compartidos</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=navurl://4fe61ddd921db378550e7d4269698f86" title="Encuesta de satisfacción www.medellin.gov.co">Encuesta de satisfacción</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=contenido/8493-Gabinete-Municipal" title="Gabinete Municipal">Gabinete Municipal</a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://csc.medellin.gov.co/APRUsers/" title="Gestionar usuario" target="_blank">Gestionar usuario </a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=navurl://ee04983747a42a53ba135f1ae3f54999">Glosario </a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=navurl://c350f8887ce51b88cff1b2149dc442b6">Manual de identidad gráfica</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/go/km/docs/pccdesign/medellin/Temas/AtencionCiudadana/Publicaciones/Shared%20Content/Documentos/2018/EstadisticasEneroJunio2018.pdf" target="_blank"></a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/portal/medellin?NavigationTarget=navurl://ee04983747a42a53ba135f1ae3f54999"></a>
                &nbsp;&nbsp;&nbsp;
                <a href="/irj/go/km/docs/pccdesign/medellin/Temas/AtencionCiudadana/Publicaciones/Shared%20Content/Documentos/2018/EstadisticasEneroJunio2018.pdf" target="_blank">Monitoreo y desempeño del sitio</a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://mercurio.medellin.gov.co/mercurio/" target="_blank">Sistema de Gestión Documentral - SGD Mercurio</a>
              </p>
            </div>

            <div>
              <p>
                <strong>Sitios web para niños:</strong>
                <br />
                <a href="http://aeropuertoolayaherrera.gov.co/informacion-para-ninos/" title="Aeropuerto Olaya Herrera" target="_blank">Aeropuerto Olaya Herrera </a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://www.metropol.gov.co/portal-de-ninos/Paginas/inicio.aspx" title="Área Metropolitana del Valle de Aburrá" target="_blank">Área Metropolitana del Valle de Aburrá </a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.cgm.gov.co/cgm/Paginaweb/P%C3%91/SitePages/Portal%20Ni%C3%B1os.aspx" title="Contraloría General de Medellín" target="_blank">Contraloría General de Medellín </a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.edu.gov.co/portal-de-ninos-y-ninas" title="Portal para niños Empresa de Desarrollo Urbano" target="_blank">EDU </a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.emvarias.com.co/culturadelaseo/home/educacion-ciudadana/linda-calle-y-pepe" title="Emvarias" target="_blank">Emvarias </a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://www.inder.gov.co/niños" title="Inder" target="_blank">Inder </a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://www.metroparques.gov.co/portal-de-ninos/index.html" title="Portal para niños Metroparques" target="_blank">Metroparques </a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://www.metrodemedellin.gov.co/Portals/0/juegos/index.html" title="Metro de Medellín" target="_blank"> Metro de Medellín </a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.personeriamedellin.gov.co/index.php/historico-de-boletines/344-informacion-para-ninos-ninas-y-adolescentes" title="Personería de Medellín" target="_blank"> Personería de Medellín </a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://metroplus.gov.co/transparencia/atencion-al-ciudadano/portal-de-ninos/" title="Portal para niños Metroplús SA" target="_blank"> Metroplús </a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.terminalesmedellin.com/sin-categoria/juega-en-la-terminal/" title="Portal para niños Terminales Medellín" target="_blank">Terminales Medellín</a> -
                &nbsp;&nbsp;&nbsp;
                <a href="http://wsp.presidencia.gov.co/Portal/Especiales/Documents/especial_ninos/index.html" title="Portal para niños Presidencia de la República" target="_blank">Presidencia</a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.mintic.gov.co/ninos/613/w3-channel.html" title="Portal para niños Ministerio de Tecnologías de la Información y las Comunicaciones" target="_blank">Mintic</a>
                &nbsp;&nbsp;&nbsp;
                <a href="https://antioquia.gov.co/gerencia-de-infancia-adolescencia-y-juventud" title="Portal para Niños Gobernación de Antioquia" target="_blank">Antioquia</a>
                <br />
                <br /> © 2021 / NIT: 890905211-1 / Código DANE: 05001 / Código Postal: 050015
                <br />
              </p>
              <p class="pFechaAct">Fecha de Actualización: 7 de Abril de 2021</p>
            </div>

          </div>

        </section>



        <section className='footer-section-3'>


          <ul class="contact-lines">
            <li>
              <img src={chatImg} alt="" />
              <p>Chat
                <br />
                <strong>
                  <a href="#" target="_self">Alcaldía de Medellín</a>
                </strong>
              </p>
            </li>
            <li>
              <FontAwesomeIcon icon={faHeadset} />
              <p>
                <span>Línea de Atención</span>
                <br />
                <strong>
                  <a href="tel:+5744444144" target="_blank">(574) 44 44 144</a>
                </strong>
              </p>
            </li>
            <li>
              <img src={phonoImg} alt="" />
              <p>Línea Gratuita Nacional
								<br />
                <a href="tel:+5701800041144" target="_blank">
                  <strong>01 8000 411 144</strong>
                </a>
              </p>
            </li>
            <li>
              <img src={handsImg} alt="" />
              <p>Centro de Relevos
                <br />
                <a href="https://web.emtelco.co/webrtcdemo/centro_relevos_mmed_VC/home.html" target="_blank">
                  <strong>
                    <span>Atención por lengua de señas</span>
                  </strong>
                </a>
              </p>
            </li>
            <li>
              <img src={intranetImg} alt="" />
              <p>Intranet
                <br />
                <a href="#">
                  <strong>Intranet</strong>
                </a>
              </p>
            </li>
          </ul>


        </section>



        <section className='footer-section-4'>

          <a href="/irj/portal/medellin?NavigationTarget=contenido/6673-Radio-institucional" title="Ir a la radio" target="_blank">
            <img src={radioImg} alt="" />
          </a>
          <a href="#" title="Ir al periodico institucional"></a>
          <a href="#" title="Ir al Correo Remoto"></a>
          <a href="https://correo.medellin.gov.co/owa" title="Ir a Correo Remoto" target="_blank">
            <img src={correoremotoImg} alt="" />
          </a>
          <a href="https://www.asocapitales.co" title="Ir a Asociación Colombiana de Ciudades Capitales - Asocapitales" target="_blank">
            <img src={asocapitalesImg} alt="" />
          </a>
          <a href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html" title="Ir a Gobierno en Línea" target="_blank">
            <img src={gob_lineaImg} alt="" />
          </a>
          <a href="http://www.colombiacompra.gov.co" title="Ir a Colombia Compra Eficiente" target="_blank">
            <img src={compra_eficienteImg} alt="" />
          </a>
          <a href="http://horalegal.inm.gov.co" title="Ir a la Hora legal" target="_blank">
            <img src={col_timeImg} alt="" />
          </a>

        </section>



        <section className='footer-section-5'>
          <div className='section-5-1'>
            <div>
              <a href='https://www.gov.co/' alt=''>
                <img src={logoCol} alt='' className='sect-5-img-1'/>
              </a>
            </div>
            <div>
              <a href='https://www.gov.co/' alt=''>
                <img src={logoGov} alt='' className='sect-5-img-2'/>
              </a>
            </div>
          </div>
          <a href='https://www.gov.co/'>Conoce GOV.CO aquí</a>
        </section>



      </div>
    </footer>
  )
}